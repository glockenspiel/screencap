package capture;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Robot;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.Timestamp;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class ScreenCapture {
	static int x=0;
	static int y=0;
	static int width=300;
	static int height=300;
	
  public static void main(String args[]) throws
           AWTException, IOException {
	  
	  TransparentPanel frame = new TransparentPanel();
	  
	  boolean flag=true;
	  boolean start=false;
	  while(flag){
		  if(frame.getHeld()){
			  start=true;
		  }
		  else{
			  if(start) flag=false;
			  
		  }
	  }
	  
	  int[] r = frame.getDimensions();
	  Rectangle rect = new Rectangle( r[0], r[1], r[2], r[3]);
	BufferedImage screencapture = new Robot().createScreenCapture(rect);
	  
	System.out.println(rect);
	  
	  //JFrame frame = new JFrame("FrameDemo");
	  //frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	  /*
	  frame.setAlwaysOnTop(true);
	  frame.setLocation(0, 0);
	  frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
	  frame.setUndecorated(true);
	  frame.setBackground(new Color(0, 255, 0, 0));
	  frame.getContentPane().setBackground(Color.BLACK);
	  frame.setLayout(new BorderLayout());
	  */
	  //frame.setUndecorated(true);
	 // frame.setContentPane(new JLabel(new ImageIcon("images.screencapture.jpeg")));
	 // frame.setLayout(new FlowLayout());
	  
	  //frame.setBackground(new Color(0,0,0,125));
	  //frame.setVisible(true);
	  
	  
     // capture the whole screen
     //BufferedImage screencapture = new Robot().createScreenCapture(
     //      new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()) );
     
   //To capture a specific area
//this is the code
     //BufferedImage screencapture = new Robot().createScreenCapture(
    //		   new Rectangle( x, y, width, height));

     
     //To capture a specific visual object
     /*
     BufferedImage image = new Robot().createScreenCapture( 
    		   new Rectangle( myframe.getX(), myframe.getY(), 
    		                  myframe.getWidth(), myframe.getHeight() ) );
     */

     // Save as JPEG
     File file = new File("screencap"+".jpg");
     ImageIO.write(screencapture, "jpg", file);

     // Save as PNG
     // File file = new File("screencapture.png");
     // ImageIO.write(screencapture, "png", file);
     
     
  }
  

}