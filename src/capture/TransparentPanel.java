package capture;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
class TransparentPanel extends JFrame
{
JPanel p1,p2;
private boolean held=false;
public boolean getHeld(){return held;}
//private int[] points=new int[4];
ArrayList<Integer> pts = new ArrayList<Integer>();

    public TransparentPanel()
    {
        createAndShowGUI();
        
        addMouseListener(new MouseAdapter() {
        	
            @Override
            public void mousePressed(MouseEvent e) {
            	held=true;
            	mousePos();
            	System.out.println("pressed: " + pts.get(0) +","+ pts.get(1));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	held=false;
            	mousePos();
            	System.out.println("released: " + pts.get(2) +","+ pts.get(3));
            }
            
            private void mousePos(){
            	int[] pos = new int[2];
            	PointerInfo a = MouseInfo.getPointerInfo();
            	Point b = a.getLocation();
            	pts.add((int) b.getX());
            	pts.add((int) b.getY());
            }
            
        });
    }
    
    public int[] getDimensions(){
    	int[] rect = new int[4];
    	
    	rect[0]=pts.get(0);
    	rect[1]=pts.get(1);
    	
    	if(pts.get(0)>pts.get(2)) rect[0]=pts.get(2);
    	if(pts.get(1)>pts.get(3)) rect[1]=pts.get(3);
    	
    	rect[2]=Math.abs(pts.get(0)-pts.get(2));
    	rect[3]=Math.abs(pts.get(1)-pts.get(3));
    	return rect;
    }
    
    private void createAndShowGUI()
    {
        // Set title and default close operation
        setTitle("Transparent Panel");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        // Set a background for JFrame
        setContentPane(new JLabel());
        
        // Set some layout, say FlowLayout
        setLayout(new FlowLayout());
        
        /*
        // Create a JPanel
        p1=new JPanel();
        
        // Set the background, black with 125 as alpha value
        // This is less transparent
        p1.setBackground(new Color(0,0,0,125));
        
        // Create another JPanel
        p2=new JPanel();
        
        // This is more transparent than the previous
        // one
        p2.setBackground(new Color(0,0,0,65));
        
        // Set some size to the panels
        p1.setPreferredSize(new Dimension(250,150));
        p2.setPreferredSize(new Dimension(250,150));
        
        // Add the panels to the JFrame
        //add(p1);
       // add(p2);
        */
        // Set the size of the JFrame and
        // make it visible
        
        
        
        setSize(600,400);
        //setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setUndecorated(true);
        setBackground(new Color(0,0,0,50));
        setVisible(true);
    }
    
    
    public static void main(String args[])
    {
        // Run in the EDT
        SwingUtilities.invokeLater(new Runnable(){
            public void run()
            {
                new TransparentPanel();
            }
        });
    }
    
    protected void paintComponent(Graphics g) {
	    super.paintComponents(g);  
	    g.drawRect(10,10,20,20);  
	   // g.setColor(Color.RED);  
	   // g.fillRect(10,10,20,20);  
	  }
    
    

}
